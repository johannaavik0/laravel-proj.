<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use function PHPUnit\Framework\returnValue;

class ProductsController extends Controller
{
    

    public function index()
    {
        return view('products/index', [
            'products' => Products::with('category')->get(),
        ]);
    }


    public function create()
    {
        return view('products/add',[
            'category' => Category::all()
        ]);
    }



    public function store(Request $request)
    {
        //valideerime vormi väljad.
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'category' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        //salvestame pildi images kausta ja saame vastu pildi asukoha failisüsteemis
        $file = $request->file('image');
        $path = Storage::putFile('images', $file);
        //näide: $path = "/images/minu-pilt.png"

        //salvestame toote andmebaasi
        Products::create([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'category_id' => $request->category,
            'image' => $path,
        ]);
        //suuname kasutaja tagasi vomi täitmise lehele 
        //(NB! võib saata tagasi ka teate, et salvestamine õnnestus )
        return redirect()->back()->with('message','Toode salvestatud');


    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $products = Products::find($id);
        return view('products/edit',[
            'products' => $products,
            'category' => Category::all()
        ]);
    }

    public function update(Request $request)
    {
        $products = Products::find($request->id);
        $request->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'category' => 'required',
            'description' => 'required',
        ]);
        $file = $request->file('image');
        $path = $products->image;
        if(!empty($file)){
            Storage::delete($products->image);
            $path = Storage::putFile('images', $file);
        }
        $products->update([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'category_id' => $request->category,
            'image' => $path,
        ]);
        return redirect()->route('products.index');
    }


    public function destroy(Request $request)
    {
        $products = Products::find($request->id);
        Storage::delete($products->image);
        $products->delete();
        return redirect()->back();
    }
}
