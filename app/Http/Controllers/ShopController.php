<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class ShopController extends Controller
{
    public function index()
    {
        return view('index', [
            'products' => Products::with('category')->get(),
        ]);
    }

    public function cart ()
   {
        $cartSum =collect(session('cart'))->reduce(function ($carry,$item){
          return $carry + ($item->quantity * $item->price);
        });
          return view('cart', [
            'cart' => session('cart') ?? [],
            'total' => $cartSum
      ]);
    }

    public function cartAdd(Request $request,Products $products)
    {
    $validator = Validator::make($request->all(),[
         'qty' => 'required|numeric|gt:0'
    ])->passes();
    $qty = $validator ? $request->qty : 1;
    $products->quantity = $qty;
    if (session()->has('cart.'.$products->id)){
        $cart_products = session('cart.'.$products->id,$products);
        $cart_products->quantity += $qty;
        session()->put('cart.'.$products->id,$cart_products);
    }else{
       session()->put('cart.'.$products->id,$products );
    }
    return response('Products added to cart');
    
}

    public function cartUpdate (Request $request, Products $products)
     {
    
        $validator = Validator::make($request->all(),[
         'qty' => 'required|numeric|gt:0'
    ])->passes();

    if (session()->has('cart.'.$products->id) && $validator){
        $cart_products = session('cart.'.$products->id,$products);
    //$cart_products->quantity += $qty;
        session()->put('cart.'.$products->id,$cart_products);
    }
    return response('Products updated');
}

public function pay (Request $request)
    {
       $request->validate([
        'name' => 'required',
        'email' => 'required|email',
       ]);
       $cartSum = collect(session('cart'))->reduce(function ($carry, $item){
         return $carry + ($item->quantitiy * $item->price);
        });
        Stripe::setApiKey(config('services.stipe.sk'));
        $paymentIntent = PaymentIntent::create([
            'amount' =>intval($cartSum * 100),
            'currency' => 'eur',
            'payment_method_types' => ['card'],
        ]);
        return[
            'paymentIntent' => $paymentIntent
        ];
       }

    public function sucess(){
        session()->forget('cart');
        return response ('success');
    }

}
//public function api(){
//    return Storage::disk('public')->get('CasperSchwartz.md');
//}

//public function apiRendered(MarkdownRenderer $renderer){
//    $markdown = Storage::disk('public')->get('CasperSchwartz.md');
//    return $renderer->toHtml($markdown);
//   }
