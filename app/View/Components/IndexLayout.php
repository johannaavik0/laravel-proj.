<?php
namespace App\View\Components;

use Illuminate\View\Component;

class IndexLayout extends Component
{
    /**
     * Get the view /contents that represents the component,
     * 
     * @return7\Iluminate\View\View
     */
    public function render()
    {
        $cart = session('cart') ?? 0;
        return view('layouts.index',[
            'cart' =>collect($cart)->sum('quantity')
        ]);
    }
}