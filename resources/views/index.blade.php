<!DOCTYPE html>
<html lang="en">
<head> 
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Hausser Keraamika</title>
<script>
    document.body.classList.add('fade');

  document.addEventListener("DOMContentLoaded", () => {
      window.setTimeout(function() {
        document.body.classList.remove('fade');
      }, 230);
    });
 </script>
<style>

html {
  scroll-behavior: smooth;
}

* {
  box-sizing: border-box;
  text-align: -webkit-center;
}
.house{
  margin-left: 40px;
}
body { 
  margin: 0;
  font-family: 'Italiana-Regular';font-size: 22px;
  opacity: 1;
  transition-duration: 2.5s;
  transition-property: opacity;
  background-color: rgb(195, 180, 160);
}

body.fade {
    opacity: 0;
  }

h1 {
  margin-left: 3rem;
  font-size: 20px;
  font-family: 'Hiragino Mincho ProN';
  font-weight: 300;
  max-width: max-content;
  padding-right: 50px;
}

.logo {
  display: block;
  margin-left: 30px;
  width: 5%;
}

.menu {
  background-color: rgb(195, 180, 160);
  height: 80px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-family: 'Italiana-Regular';
  padding-right: 100px;
}

.name {
  padding: 30px;
  width: 300px;
  height: 30px;

}

div2{
  padding-top: 30px;
}

button {
  border: 500px;
  text-align: center;
  padding: 12px;
  text-decoration: none;
  font-size: 23px;
  font-weight: 400px;
  font-family: Didot;
  line-height: 25px;
  border-radius: 4px;
  float: right;
  margin: 3px;
  background-color: transparent;

}
.Products {
  height: 100px;
  opacity: 4;
  padding: 25px;
  text-align: -webkit-center;
  border-radius: 3px;

}
.button2 {
  border: 500px;
  text-align: center;
  padding: 12px;
  font-size: 23px;
  font-weight: 400px;
  font-family: Didot;
  line-height: 25px;
  border-radius: 4px;
  float: center;
  margin: 30px;
  background-color: rgb(195, 180, 160);
  width: 30px;
}

div.Products {
  padding-top: 100px;
  text-align: center;
  padding-bottom: 100px;
  font-family: Didot;
  font-weight: 20px;
  font-size: 30px;
  background-color: rgb(195, 180, 160);
}

div.name {
  padding-bottom: 30px;
  text-align: center;
  align-items: center;
}

a {
  /*background-color: Salmon;*/
  text-decoration: none;
  text-decoration-color: indianred;
  padding: 50px;
  color: black;
  font-family: Didot;
}

.header a.logo {
  font-size: 25px;
  font-weight: bold;
  align-items: left;
}

.About{
  font-size: larger;
  text-align: left;
  padding: 120px;
  box-shadow: 0px 8px 5px rgba(0, 2, 0, 0.1);
  background-color: rgb(195, 180, 160);
}
.img.About{
  width: 80px;
  height: 80px;
}

.OUR.MAKER {
  text-align: right;
}
.info {
  text-align: right;
  width: 30px;
}

.conteiner2{
  background-color: rgb(195, 180, 160);
  
}
.conteiner3{
  display: left;
  background-color: rgb(195, 180, 160);
  gap: 70px;
  padding: 30px;
}
.figure{
  display: left;
  padding: 5px;

}

figcaption{
  align-items: center;
  font-size: 30px;
  font-weight: 300;
  font-family: 'Times New Roman', Times, serif;
  background-color: rgb(195, 180, 160);
}

.conteiner{
  width: 100%;
  background-color: rgb(195, 180, 160);
}
.block {
  justify-content: center;
  padding: 40px;
  background-color: rgb(195, 180, 160);
}

text{
  padding-bottom: 50px;
  padding-left: 50px;
  padding-right: 50px;
  color: black;
}

.text1{
  align-items: center;
  padding-bottom: 70px;
  font-size: 40px;
}

.galerii {
  grid-template-columns: 1fr 1fr 1fr;
  width: 600px;
  gap: 30px;
  align-items: center;
  padding-bottom: 30px;
}
.p{
  color: #5f656e;
  padding-bottom: 50px;
}

p2{
text-align: right;
padding-bottom: 70px;
padding-top: 50px;
padding-left: 80px;
font-size: 25px;
color: grey;
}

p1{
text-align: left;
padding-bottom: 70px;
padding-top: 50px;
font-size: 25px;
padding-right: 70px;
color: grey;

}

.text2 {
  align-items: center;
  padding-bottom: 20px;
  font-size: 40px;
  padding-top: 50px;
}

.company{
padding: 20px;
padding-left: 50px;
text-align: center;
}

.all{
  font-size: 40px;
  background-color: whitesmoke;
}
</style>

</head>

<body>

<div class="menu">
  <div class="house">
    <svg xmlns= "http://www.w3.org/2000/svg" version="1.0" width="69px" height="100px" viewBox="0 0 390.000000 200.000000" preserveAspectRatio="xMidYMid meet">
    <g transform="translate(0.000000,712.000000) scale(0.050000,-0.050000)" fill="#000004" stroke="none">
    <path d="M4484 13682 c-1014 -407 -1126 -457 -1138 -502 -7 -27 -167 -252 -356 -500 -189 -247 -782 -1025 -1318 -1727 -1043 -1366 -1009 -1319 -972 -1363 59 -71 108 -36 271 189 88 120 166 219 175 220 8 0 14 -770 13 -1712 -2 -1777 -3 -1746 75 -1747 13 0 35 12 50 27 23 23 31 23 52 0 41 -44 3889 -40 3925 4 24 29 28 29 43 0 20 -35 87 -42 107 -10 7 12 25 17 40 11 36 -14 2322 1358 2350 1410 14 27 21 448 20 1318 l-2 1279 100 51 c56 28 101 59 101 68 0 10 22 23 49 30 53 13 83 76 57 117 -110 178 -2320 3241 -2344 3248 -17 6 -48 18 -67 28 -59 32 -74 26 -1231 -439z m2345 -1292 l1128 -1579 -84 -33 c-45 -18 -159 -68 -253 -112 -146 -69 -1445 -677 -1817 -850 -69 -33 -130 -56 -134 -52 -4 4 -105 169 -225 367 -120 197 -346 571 -504 829 -157 259 -345 569 -419 690 -73 121 -307 505 -519 854 -213 348 -388 639 -389 645 -4 16 2031 830 2062 825 14 -2 533 -715 1154 -1584z m-3294 585 c70 -127 1569 -2593 1637 -2694 l48 -71 -980 -5 c-539 -3 -1421 -3 -1959 0 l-979 5 294 383 c1034 1346 1818 2379 1844 2429 37 71 27 76 95 -47z m4145 -3681 l0 -1227 -1055 -634 c-580 -349 -1084 -653 -1120 -675 l-65 -42 0 1552 c0 854 4 1552 9 1552 4 0 36 -49 71 -110 56 -98 70 -110 129 -110 59 0 1926 850 1980 902 10 10 26 18 35 18 9 0 16 -552 16 -1226z m-2390 -944 c0 -930 -6 -1690 -14 -1690 -7 0 -19 9 -26 20 -23 38 -3870 26 -3912 -11 -32 -30 -34 56 -36 1670 l-2 1701 1995 0 1995 0 0 -1690z"/>
    <path d="M2432 8888 c-24 -64 -13 -1339 12 -1364 34 -34 1518 -34 1552 0 34 34 34 1338 0 1372 -41 41 -1548 34 -1564 -8z m788 -673 l0 -555 -320 0 -320 0 0 555 0 555 320 0 320 0 0 -555z m640 0 l0 -555 -240 0 -240 0 0 555 0 555 240 0 240 0 0 -555z"/>
</div>
        <h1>Hausser Keraamika</h1>
        <div>
            <button>
              <a href="#about">About</a>
            </button>
            <button>
              <a href="#contact">Contact</a>
            </button>
            <x-cart-button :cart="$cart ?? ''"/>
        </div>
</div>
<div class="conteiner2">
<video width="950" height="534" autoplay muted>
 <source src="{{url('images/Vaasid.mp4')}}" type="video/mp4">
</video>
</div>
<div class="Products">
  <text>OUR HANDMADE CERAMICS</text>
</div>
<figure>
          <img class="galerii" src="{{url('images/IMG_5187.webp')}}" alt="Tassid">
            <figcaption>Hand-painted porcelain set</figcaption>
              <p>Price 60€</p>
          <img class="galerii" src="{{url('images/IMG_5275.webp')}}" alt="Kaanega kauss">
            <figcaption>A Blue porcelain Cup</figcaption>
              <p>Price  35€</p>
          <img class="galerii" src="{{url('images/IMG_5262.webp')}}" alt="Keraamiline Küünlatops">
            <figcaption>A</figcaption>
              <p>Price  60€</p>
</figure>
<div class="block">
</div>
<figure>
        <img class="galerii" src="{{url('images/IMG_5225.webp')}} " alt="Tassid">
          <figcaption>Vase</figcaption>
            <p>Price  25€</p>
         <img class="galerii" src="{{url('images/IMG_5283.webp')}}" alt="Komplekt">
          <figcaption>Sugar container with lid</figcaption>
            <p>Price  25€</p>
</figure>
</div>
<div class="all">
    <x-products-button :View All products="products ?? ''"/>
</div>
  <div class="About" id="about"> 
  <div class="text1">About</div>
  <div class="img.About">
    <img src="https://scontent-arn2-2.xx.fbcdn.net/v/t31.18172-8/27709814_415606515559921_7273364971648586107_o.jpg?_nc_cat=100&ccb=1-6&_nc_sid=09cbfe&_nc_ohc=LjaI9tw0rIMAX_-EhWN&_nc_oc=AQk2AOFYfUPJK7QduS4baeVKQkmwCUUBvGJ8HR5R_cn5JZSUrJ-5rcr6nG56krda5HE&_nc_ht=scontent-arn2-2.xx&oh=00_AT-VlXTK1k2YULJI_flUGBdrZo35eXd7qj3QA1WnfP1x-Q&oe=629DF151" alt="OUR MAKER" width="300px" height="300px"></div>
  <div class="info"></div>
    <p>Käsitööna valminud unikaalsed keraamika esemed. Dekoratiivnõud, keraamilised urnid. Portselannõud käsitsi maalitud portselanmaal. Handmade Ceramics kasitööna valminud unikaalsed dekoratiivnõud. Valmistame tellimustööde valmimisaeg kuni 3 nädalat. Vastavalt kliendi soovile.</p>
  <div class="text2" id="contact">Contact</div>
  <div class="company">Hausser Keraamika</div>
  <div class="company">Hausser Design and Construction OÜ</div>
    <a href="..">email@email.com</a>
    <a href="..">+372 **** ****</a>
    <a href="..">@Facebook</a>
    <a href="..">@Instagram</a>
  </div>
</body>