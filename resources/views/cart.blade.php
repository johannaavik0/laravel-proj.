<x-index-layout>
    <div class="max-w-screen-xl px-6 lg:px-12 mx-auto w-full h-full mt-16">
      <a class="flex max-w-min flex-nowwrap gap-2 items-center" href="/">
          <div class="text-red-600 font-bold h-4 w-4">
          </div>
          <p class="text-gray-500 whitespace-nowrap">Back to Shopping ></p>
      </a>
      <div class="grid lg:grid-cols-2 gap-6 pb-12 mt-12">
      <div class="flex flex-col divide-y-2 gap-4 mt10 border-b-2 lg:border-0">
          @foreach ($cart as $item)
          <div class="grid grid-cols-3 gap-4 row-auto sm:grid-cols-5 xl:grid-cols-6 py-2 place-item-center">
              <img class="row-span-full sm:block h-6 w-16 object-cover aspect-square" src="{{$item-image}}">
              <div class="flex flex-col">
                  <p class="text-xs text-orange-300">{{$item->category->name}}</p>
                  <p class="text-lg font-bold">{{$item->name}}</p>
          </div>
          <div class="flex px-6 flex-col">
              <p class="text-xs text-gray-500">Price</p>
              <p class="text-lg font-bold">{{$item->price}}€</p>
          </div>
          <div class="flex px-6 justify-center items-center flex-col">
              <p class="text-xs text-gray-500">Quantity</p>
              <form class="felx" methid="POST" onsubmit="event.preventDefault();return upadateItem(<?php echo($item->id) ?>, new">
              FormData(event.target))">
                 <input name="qty" type="number" class="text-lg w-14 bg-gray-100 border-0">
                 <button class="text-sm text-white font-bold bg-teal-600 px-1">update</button>
              </form>
          </div>
          <div class="flex px-6 flex-col">
              <p class="text-xs text-gray-500">Total</p>
              <p class="text-lg font-bold">{{$item->quantity * $item->price}}€</p>
          </div>
          <div class="flex px-6 flex-col">
              <p class="text-xs text-gray-500">Remove</p>
              <button on click="event.preventDefault(); return deleteItem"(<?php echo($item->id) ?>)" class="leading-tight text-xl font-bold py-1 text-teal-800 text-center align-middle">X</button>
             </form>
          </div>
      </div>
      @endforeach
      </div>
      </div>
    </div>
</x-index-layout>
<script src="https://js.stripe.com/v3/"></script>
<script>
  const updateItem = (id, form) => axios.post(`/cart-update/${id}`, form).then(res => console.log(res))
  const deleteItem = (id) => axios.post(`/cart-delete/${id}`).then(res => window.location.reload())


  const stripe = Stripe("pk_test_51KSpwYFdJ2b3zAzAU6YHdkAp9iWvOz84AWRBBvwpHLjNb5HjIXqh9vrenxmzqNIa8x8p5244CzCTVIPI01ogbzqu00R57RJif2");
  const elements = stripe.elements();
  var style = {
      base: {
        color: "#32325d",
        fontFamily: 'Arial, sans-serif',
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d"
        }
      },
      invalid: {
        fontFamily: 'Arial, sans-serif',
        color: "#fa755a",
        iconColor: "#fa755a"
      }
    };
    const card = elements.create("card", { style: style });
    card.mount("#card-element");
    const form = document.getElementById("payment-form");
    const nameError = document.getElementById('name_error')
    const emailError = document.getElementById('email_error')


    form.addEventListener("submit", async (event) =>  {
      event.preventDefault();
     const formData = new FormData(event.target)
        axios.post('/pay', formData)
          .then((res)=>{
            handleSubmit(res.data.paymentIntent)
        })
        .catch((err) => {
          err.response.data.errors?.name ? nameError.innerText = err.response.data.errors.name[0] : ''
          err.response.data.errors?.email ? emailError.innerText = err?.response.data.errors.email[0] : ''
  });
    });


const handleSubmit = async (pi) => {
  await stripe.confirmCardPayment(pi.client_secret, {
      payment_method: {
        card: card
      }
    })
    .then(res  => {
      if(res.paymentIntent.status === 'succeeded')
      axios.post('/success')
      .then(window.location.replace('/'));
  })

}

const clearErrors = (id) => document.getElementById(id).innerText = ''
</script>