<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Hausser') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body>


        <div class="font-sans min-h-screen max-screen-xl mx-auto w-full text-gray-900 antialiased grip grid-row-[60px_1_fr_100px]">
         <x-nav :cart="$cart" />
          {{ $slot }}
            <!-- Page Heading -->
            <footer class="h-full border-t-2 mx-6 border-grey-800 grid grid-cols-3">
                <div class="flex gap-2 mt-4">
                <div class="h-8 w-8 text-green-800">
                </div>
                <div class="h-8 w-8 text-green-800">
                    </div>
             </div>
                <div class="hidden lg:flex justify-center">
                    <div>
                        <h1 class="text-white bg-red-800 px-2 py-1 text-base font-bold"></h1>
                    </div>
                </div>
                <p class="text-sm mt-4 justify-self-end text-gray-500"></p>
            </footer>
        </div>
    </body>
</html>
