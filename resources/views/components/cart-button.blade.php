@props(['cart'])
<a class="relative p-2" href="{{route('cart.index')}}">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" id="Layer_1" x="0px" y="0px" width="44px" height="44px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve">
<polygon fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" points="44,18 54,18 54,63 10,63 10,18 20,18 "/>
<path fill="none" stroke="#000000" stroke-width="2" stroke-miterlimit="10" d="M22,24V11c0-5.523,4.477-10,10-10s10,4.477,10,10v13  "/>
</svg>
<div class="albsolute top-3 right-0 w-5 h-5 rounded-full flex items-center justify-center text-xs">
<i class="fa-duotone fa-bag-shopping"></i>
    {{ $cart ?? ''}}
</div>
</a>