<x-app-layout>  
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-black-300 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-brown  border-b border-yellow-200">
                    @foreach ($users as $user)
                    <p class="text-lg text-white-300 font-bold">{{$user->email}} </p>
                    @endforeach
                    You`re logged in!
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<style>
*{
    background-color: #fefbd8;
}
.bg{
  background-color: #fefbd8;
}
.py-12{
    background-color: #fefbd8;
}
.max-w-7xl{
    background-color: #fefbd8;
}
.min-h-screen{
    background-color: #fefbd8;
}
.font{
    font-family: Didot;
}


</style>
