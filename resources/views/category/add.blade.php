<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-screen-md mx-auto sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('category.submit') }}">
                @csrf
                <div>
                    <x-label for="name" :value="__('Name')" />
                    <x-input id="" class="block mt-1 w-full" type="text" name="name" :value="old('name')" />
    
                    <x-button class="ml-3">
                        {{ __('Submit') }}
                    </x-button>
            </form>
        </div>
    </div>
</x-app-layout>
