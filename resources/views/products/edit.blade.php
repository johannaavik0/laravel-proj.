<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-screen-md mx-auto sm:px-6 lg:px-8">
            <form method="POST" action="{{ route('products.update') }}" enctype="multipart/form-data">
                @csrf
                <div>
                    <input type="hidden" name="id" value="{{$products->id}}">
                    <x-label for="name" :value="__('Name')" />
                    <x-input class="block mt-1 w-full" type="text" name="name" :value="old('name') ?? $products->name" />

                    <x-label for="price" :value="__('Price')" />
                    <x-input id="" class="block mt-1 w-full" type="text" name="price" :value="old('price') ?? $products->price" />

                    <select class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="category" id="">
                        @foreach ($category as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
                    @error('category')
                        <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                    @enderror

                    <div class="mt-1">
                        <label class="text-sm" for="image">Image</label>
                        <input id="" class="block mt-1 w-full" type="file" name="image"  />
                        @error('image')
                            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="mt-1 pb-6 flex flex-col">
                        <label class="text-sm" for="description">Description</label>
                        <textarea value="{{old('description') ?? $products->description}}" class="rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="description" id="desc" cols="30" rows="5">{{old('description') ?? $products->description}}</textarea>
                        <!--Sisend name="description"-->
                        @error('description')
                            <div class="text-red-500 text-sm mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                    <x-button class="ml-3">
                        {{ __('Update') }}
                    </x-button>
            </form>
        </div>
    </div>
</x-app-layout>
