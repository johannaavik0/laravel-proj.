<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-black-300 leading-tight">
            {{ ('Products') }}
        </h2>
    </x-slot>
<figure>
      <img src="{{url('images/IMG_5187.webp')}}" alt="Tassid" img="rounded-full" width="200px" height="200px">
        <figcaption>Hand-painted porcelain set</figcaption><p>Price 60€</p>
      <button>Add to cart</button>
      <img src="{{url('images/IMG_5262.webp')}}" alt="Tassid" img="rounded-full" width="200px" height="200px">
        <figcaption>Hand-painted porcelain set</figcaption><p>Price 60€</p>
      <button>Add to cart</button>
</figure>
<figure class="row2">
      <img src="{{url('images/IMG_5225.webp')}}" alt="Tassid" img="rounded-full" width="200px" height="200px">
        <figcaption>Hand-painted porcelain set</figcaption><p>Price 60€</p>
        <button>Add to cart</button>
      <img src="{{url('images/IMG_5225.webp')}}" alt="Tassid" img="rounded-full" width="200px" height="200px">
        <figcaption>Hand-painted porcelain set</figcaption><p>Price 60€</p>
        <button>Add to cart</button>
      <img src="{{url('images/IMG_5225.webp')}}" alt="Tassid" img="rounded-full" width="200px" height="200px">
        <figcaption>Hand-painted porcelain set</figcaption><p>Price 60€</p>
        <button>Add to cart</button>
</figure>
             @foreach ($products as $item)
                        <tr class="text-center">
                            <td class="py-2">
                                <div class="flex justify-center py-2">
                                    <img class="h-14 w-14 object-cover rounded-full" src="{{$item->image}}" alt="pilt">
                                </div>
                            </td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->category->name}}</td>
                            <td>{{$item->description}}</td>
                            <td>{{$item->price}}</td>
                            <th>
                            <div class="flex gap-4">
                                <a class="bg-white-200 text-white rounded" href="{{route('products.edit',$item->id)}}">Edit</a>
                                <form method="POST" action="{{ route('products.destroy') }}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{item->id}}">
                                    <button class="py-1 px-2 bg-blue-300 text-white rounded">Delete</button>
                                </form>
                            </div>
                        </th>
                    </tr>   
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<style>
    figure{
        padding: 30px;
        display: flex;
        
    }
    figcaption{
        font-size: medium;
        font-style: normal;
        text-align: left;
        padding-top: 20px;
        padding-bottom: 8px;
    }
    p{
        font-size: medium;
        font-style: normal;
        padding-bottom: 50px;
        text-align: left;
        text-decoration-color: grey;
    }
    .row2{
        display: flex;
        padding: 30px;
    }
    .img{
        gap: 20px;
    }
</style>
</x-app-layout>

