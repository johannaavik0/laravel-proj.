<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ShopController::class, 'index']); 
Route::get('cart', [ShopController::class,'cart'])->name('cart.index');
Route::post('/cart-add/{products}', [ShopController::class,'cartAdd'])->name('cart.add');
Route::post('/cart-update/{products}', [ShopController::class,'cartUpdate'])->name('cart.update');
Route::post('/cart-delete/{products}', [ShopController::class,'destroy'])->name('cart.delete');

Route::post('/pay',[ShopController::class,'pay'])->name('pay');
Route::post('/success', [ShopController::class,'success'])->name('success');


Route::middleware('auth')->group(function() {
    Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
    //products
    Route::get('/products', [ProductsController::class, 'index'])->name('products.index');
    Route::get('/products-add', [ProductsController::class, 'create'])->name('products.add');
    Route::post('/products-submit', [ProductsController::class, 'store'])->name('products.submit');
    Route::get('/products-edit/{id}', [ProductsController::class, 'edit'])->name('products.edit');
    Route::post('/products-update', [ProductsController::class, 'update'])->name('products.update');
    Route::post('/products-delete', [ProductsController::class, 'destroy'])->name('products.destroy');
    //category
    Route::get('/category-add', [CategoryController::class, 'create'])->name('category.add');
    Route::post('/category-submit', [CategoryController::class, 'index'])->name('category.submit');
});
  

require __DIR__.'/auth.php';
