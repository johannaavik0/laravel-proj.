<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'Hausser Keraamika OÜ');
set('remote_user', 'virt98408'); 
set('http_user', 'virt98408'); 
set('keep_releases', 2);

// Hosts
host('ta20avik.itmajakas.ee')
    ->setHostname('ta20avik.itmajakas.ee')
    ->set('http_user', 'virt98408')
    ->set('deploy_path', '~/domeenid/www.ta20avik.itmajakas.ee/hausser')
    ->set('branch', 'master');

// Tasks
set('repository', 'git@gitlab.com:johannaavik0/laravel-proj..git');
//Restart opcache
task('opcache:clear', function () {
    run('killall php80-cgi || true');
})->desc('Clear opcache');

task('build:node', function () {
    cd('{{release_path}}');
    run('npm i');
    run('npm run prod');
    run('rm -rf node_modules');
});
task('deploy', [
    'deploy:prepare',
    'deploy:vendors',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'build:node',
    'deploy:publish',
    'opcache:clear',
    'artisan:cache:clear'
]);
after('deploy:failed', 'deploy:unlock');
